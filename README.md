# ACTS MCC Package

Package that provides helper tools to integrate the [ACTS](https://github.com/acts-project/acts) tracking framework with the Muon Collider Collabration software.

## Setup Instructions

### Container

All commands should be run inside the `gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9` container. Instructions on usage are in the [CERN 2023 Tutorial](https://mcd-wiki.web.cern.ch/software/tutorials/cern2023/setup/).

### Build Instructions

The following commands will build all necessary extra packages from the workspace.

```bash
source /opt/setup_mucoll.sh
cmake -S . -B build/
cmake --build build/
```

If running the geantino scan, you will need to build with debug symbols. For reasons unknown, this is required to save more than one event as output.

```bash
source /opt/setup_mucoll.sh
cmake -S . -B build_dbg/ -DCMAKE_BUILD_TYPE=Debug
cmake --build build_dbg/
```

Note that the debug build will not work with programs that require the loading of the TGeo geometry due to performance issues.

### Setup Script

The included `setup.sh` script is useful for defining all paths for the binaries built by the workspace. At the current stage, it setups the following:

- ILC software via `init_ilcsoft.sh`
- External binaries/libraries found in `exts`
- `MYBUILD` enviromental variable pointing to the build directory
- `MYWORKSPACE` enviromental variable pointing to the clone of this repository

Run the following at the start of every session. It has an optional argument to the build directory and is set to `build/` by default.

```shell
source setup.sh [build]
```

## Repository Structure

- `exts/` Custom external packages.
- `response/` Common response files for use with ACTS examples.
- `data/` Data files with example geometries.

## Example Commands

### Exporting TGeo

The TGeo geometry is used by ACTS to build its internal representation of the tracking detector.

```shell
${MYBUILD}/dd2tgeo ${MUCOLL_GEO} test.root
```

### Simulated Hits

```shell
${MYBUILD}/exts/acts/bin/ActsExampleFatrasTGeo --geo-tgeo-filename ${MYBUILD}/data/MuColl_v1.root --geo-tgeo-jsonconfig ${MYBUILD}/data/MuColl_v1.json --output-root 1 --events 1000
```

### Truth Tracking

```shell
${MYBUILD}/exts/acts/bin/ActsExampleFatrasTGeo --geo-tgeo-filename ${MYBUILD}/data/MuColl_v1.root --geo-tgeo-jsonconfig ${MYBUILD}/data/MuColl_v1.json --response-file ${MYBUILD}/response/tgeo-mat-lctracker.response --bf-values 0 0 3.57 --output-dir OUT_mu --output-csv 1 -n 1000
${MYBUILD}/exts/acts/bin/ActsExampleTruthTracksTGeo --geo-tgeo-filename ${MYBUILD}/data/MuColl_v1.root --geo-tgeo-jsonconfig ${MYBUILD}/data/MuColl_v1.json --response-file ${MYBUILD}/response/tgeo-mat-lctracker.response --bf-values 0 0 3.57 -n 1000
```

## ACTS Build Settings

The following optional ACTS components are automatically enabled to get access to the internal ACTS tools and necessary plugins. ACTS v13.0.0 is included.

- `ACTS_BUILD_FATRAS`
- `ACTS_BUILD_PLUGIN_TGEO`
- `ACTS_BUILD_PLUGIN_DD4HEP`
- `ACTS_BUILD_EXAMPLES`
- `ACTS_BUILD_EXAMPLES_DD4HEP`
- `ACTS_BUILD_EXAMPLES_TGEO`
- `ACTS_BUILD_EXAMPLES_GEANT4`

The ACTS is also modified to use an envelope of 0.1 mm. This is needed to prevent the doublet layers to be identified as overlapping. This modification is needed for the ACTSTracking as the setting can be changed directly.

## lcgeo Modification

The package is based on the `v00-18-01-MC` tag of the [lcgeo](https://github.com/MuonColliderSoft/lcgeo.git) package. The following modifications have been made to support loading of the exported geometry using ACTS.

- The `CMakeLists.txt` locates the include paths using `CMAKE_CURRENT_SOURCE_DIR` instead of `CMAKE_SOURCE_DIR` to compile as a submodule.
- The `VertexEndcap_o1_v06_geo`, `TrackerBarrel_o1_v05_geo`, `TrackerEndcap_o2_v06_geo` classes have been modified to name the sensitive part of the module stackup as `sensor#` instead of `component#`. This is needed by ACTS to identify the sensor inside the geometry.