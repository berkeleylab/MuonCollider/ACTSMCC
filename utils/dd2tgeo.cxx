#include <DD4hep/Detector.h>

#include <iostream>

#include <TFile.h>

int main(int argc, char* argv[])
{
  if(argc!=3)
  {
    std::cerr << "usage: " << argv[0] << " input.xml output.root" << std::endl;
    return 1;
  }

  //
  // Options
  std::string input =argv[1];
  std::string output=argv[2];

  //
  // Create and load detector
  std::shared_ptr<dd4hep::Detector> detr(&dd4hep::Detector::getInstance());

  detr->fromCompact(input);

  TGeoManager &m=detr->manager();
  TFile *fh=TFile::Open(output.c_str(), "RECREATE");
  m.Write();
  fh->Close();

  return 0;
}
