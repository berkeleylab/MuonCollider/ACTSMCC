# - Try to find lcgeo
# Once done this will define
#  lcgeo_FOUND - System has libftdi1
#  lcgeo_LIBRARIES - The libraries needed to use libftdi1

FIND_LIBRARY(lcgeo_LIBRARY NAMES liblcgeo lcgeo
  HINTS /usr/lib64 /usr/local/lib /usr/lib/x86_64-linux-gnu )

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set lcgeo_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(lcgeo  DEFAULT_MSG
  lcgeo_LIBRARY )

MARK_AS_ADVANCED( lcgeo_LIBRARY )

SET(lcgeo_LIBRARIES ${lcgeo_LIBRARY} )
